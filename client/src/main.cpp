#include <thread>
#include <set>
#include <sstream>
#include <iostream>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Options.hpp>


extern "C" {
#include <zmq.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "logger.h"
#include "loggerconf.h"
};


void initialization() {
	logger_initConsoleLogger(NULL);
	logger_initFileLogger("log_client.txt", 100 * 100, 1);
	logger_autoFlush(100);
	logger_setLevel(LogLevel_INFO);
}


void getToken(const std::string& a, const std::string& b, std::string& c) {
	curlpp::Cleanup myCleanup;
	std::ostringstream os;
	os << curlpp::options::Url(std::string("http://127.0.0.1:8080/auth/") + a + '/' + b); // /auth/admin1/test
	c = os.str();
}

void registration(std::string a, std::string b) {
	curlpp::Cleanup myCleanup;
	std::ostringstream os;
	os << curlpp::options::Url(std::string("http://127.0.0.1:8080/registration/") + a + '/' + b);
    std::cout << os.str() << std::endl;
}

int main(int argc, const char** argv) {
	initialization();
	LOG_INFO("client has started");

    LOG_INFO("for registration input - \"1\", for auth input \"2\"");
    int f;
    std::cin >> f;
    std::string log, pass;

    LOG_INFO("login: ");
    std::cin >> log;

    LOG_INFO("password: ");
    std::cin >> pass;

    std::string token;


    if (f == 1) {
        registration(log, pass);
    }
    getToken(log, pass, token);

    LOG_INFO("%s", token.c_str());


}
