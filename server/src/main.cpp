#include <httpserver.hpp>

#include <iostream>
#include <memory>
#include <unordered_map>
#include <fstream>

extern "C" {
    #include "logger.h"
    #include "loggerconf.h"
};

using namespace httpserver;

std::unordered_map<std::string, std::string> Users; // <login, pass> 
std::unordered_map<uint64_t, std::string> Tokens; // <token, login>

std::string users("/Users/a1/users.txt");


class registration_source : public http_resource {
    std::ofstream out;
public:
    registration_source() {
                out.open(users, std::ios::app);
    }

    ~registration_source() {
        out.close();
    }

    const std::shared_ptr<http_response> render_PUT(const http_request& req) {
        
        std::string login(req.get_arg("login"));
        std::string pass(req.get_arg("pass"));
        if (login.empty() or pass.empty()) {
            return std::shared_ptr<http_response>(new string_response("Registration NOT OK\nlogin or pass empty", 400));
        }

        if (Users.find(login) == Users.end()) {
            Users[login] = pass;
            out << login << ' ' << pass << std::endl;
            LOG_INFO("successful registration of a user with a login: %s", login.c_str());
        } else {
            return std::shared_ptr<http_response>(new string_response("Registration NOT OK\nsuch loging already exists", 400));
        }
        return std::shared_ptr<http_response>(new string_response("Registration OK", 200));
    }
};



class auth_source : public http_resource {
public:
    const std::shared_ptr<http_response> render_GET(const http_request& req) {

        std::string login(req.get_arg("login"));
        std::string pass(req.get_arg("pass"));

        if (login.empty() or pass.empty()) {
            return std::shared_ptr<http_response>(new string_response("Auth NOT OK\nlogin or pass empty", 400));
        }


        auto tmp = Users.find(login);
        if (tmp != Users.end()) {
            if (tmp->second == pass) {
                uint32_t ss = std::hash<std::string>{}(login + pass);
                Tokens[ss] = login;
                LOG_INFO("successful user authentication with login: %s", login.c_str());
                return std::shared_ptr<http_response>(new string_response(std::to_string(ss), 200));
            } else {
                return std::shared_ptr<http_response>(new string_response("Auth NOT OK\nwrong passowd", 400));
            }
        } else {
            return std::shared_ptr<http_response>(new string_response("Auth NOT OK\nno such login", 400));
        }
    }
};






class users_source : public http_resource {
public:
    const std::shared_ptr<http_response> render_GET(const http_request& req) {
        std::string ss = req.get_header("token");
        if (ss.empty()) {
            return std::shared_ptr<http_response>(new string_response("no token", 400));
        }
        uint64_t token = std::atoll(ss.c_str());
        auto tmp = Tokens.find(token);
        if (tmp == Tokens.end()) {
            return std::shared_ptr<http_response>(new string_response("not authorized", 400));
        }
        std::string s("{\"Users\":[");
        for (auto i : Users) {
            s += "\"" + i.first + "\"";
            s += ", ";
        }
        auto i = s.rbegin();
        *i++ = '}';
        *i = ']';

        LOG_INFO("successful request /users from user: %s", tmp->second.c_str());
        return std::shared_ptr<http_response>(new string_response(s, 200));
    }
};

void initialization() {
    logger_initConsoleLogger(NULL);
	logger_initFileLogger("/Users/a1/log.txt", 100 * 100, 1);
	logger_autoFlush(100);
	logger_setLevel(LogLevel_TRACE);

    std::ifstream in;         // поток для записи
    in.open(users);
    std::string log, pass;
    while (in >> log >> pass) {
        Users[log] = pass;   
    }
    in.close();
    return;
}



int main(int argc, char** argv) {
    initialization();
    webserver ws = create_webserver(8080);
    registration_source hwr;
    hwr.disallow_all();
    hwr.set_allowing("PUT", true);
    ws.register_resource("/registration/{login}/{pass}", &hwr);

    auth_source auth;
    auth.disallow_all();
    auth.set_allowing("GET", true);
    ws.register_resource("/auth/{login}/{pass}", &auth);

    
    users_source users;
    users.disallow_all();
    users.set_allowing("GET", true);
    ws.register_resource("/users", &users);
        
    ws.start(true);
    return 0;
}
